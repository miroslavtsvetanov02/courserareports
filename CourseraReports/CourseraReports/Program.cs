﻿using System.Data;
using Microsoft.Data.SqlClient;
using System.Collections.Generic;
using System;

namespace CourseraReports
{
    public class Program
    {
        static void Main(string[] args)
        {
			string fileLocation;

			Console.WriteLine("Please enter the file location you want to save the file to:");

			while (string.IsNullOrEmpty(fileLocation = Console.ReadLine()) || !System.IO.Directory.Exists(fileLocation))
			{
				Console.WriteLine("Please enter a valid file location you want to save the file to:");
			}

			Console.WriteLine("Do you want to save the file as a CSV or HTML? (CSV/HTML) or for both (Both)");
			string reportType = Console.ReadLine();

			switch (reportType.ToUpper())
			{
				case "CSV":
					GetCSV(fileLocation);
					break;
				case "HTML":
					GetHTML(fileLocation);
					break;
				case "Both":
					GetCSV(fileLocation);
					GetHTML(fileLocation);
					break;
				default:
					GetCSV(fileLocation);
					GetHTML(fileLocation);
					break;
			}
		}

        private static string GetHTML(string filelocation)
        {
            using (SqlConnection cn = new SqlConnection(GetConnectionString()))
            {
                cn.Open();
                return CreateHTML(new SqlCommand(
					"SELECT CONCAT(s.first_name, ' ', s.last_name) AS Student, SUM(c.credit) AS TotalCredit, c.name AS Course, c.total_time AS TotalTime, c.credit AS Credit, CONCAT(i.first_name, ' ', i.last_name) AS Instructor \r\n" +
					"FROM dbo.students AS s\r\n" +
					"LEFT JOIN dbo.students_courses_xref AS scx ON s.pin = scx.student_pin\r\n" +
					"LEFT JOIN dbo.courses AS c ON scx.course_id = c.id\r\n" +
					"LEFT JOIN dbo.instructors AS i ON c.instructor_id = i.id\r\n" +
					"GROUP BY s.pin, s.first_name, s.last_name, s.time_created, c.name, c.total_time, c.credit, c.instructor_id, i.first_name, i.last_name\r\n", cn).ExecuteReader(), filelocation);
            }
        }

        private static string CreateHTML(IDataReader reader, string path)
        {
			string file = path + @"\report.html";
			List<string> lines = new List<string>();

            string[] columns = new string[reader.FieldCount];

            for (int i = 0; i < reader.FieldCount; i++)
            {
                columns[i] = reader.GetName(i);
            }

			// Create the HTML table header
			lines.Add("<table>");
			lines.Add("<tr>");
			foreach (string column in columns)
			{
				lines.Add("<th>" + column + "</th>");
			}
			lines.Add("</tr>");


			while (reader.Read())
            {
                object[] values = new object[reader.FieldCount];

                reader.GetValues(values);
				lines.Add("<tr>");
				foreach (object value in values)
				{
					lines.Add("<td>" + value.ToString() + "</td>");
				}
				lines.Add("</tr>");
		
			}

			lines.Add("</table>");

			System.IO.File.WriteAllLines(file, lines);

            return file;
        }

        private static string GetCSV(string filelocation)
        {
            using (SqlConnection cn = new SqlConnection(GetConnectionString()))
            {
                cn.Open();
                return CreateCSV(new SqlCommand(
                    "SELECT CONCAT(s.first_name, ' ', s.last_name) AS Student, SUM(c.credit) AS TotalCredit, c.name AS Course, c.total_time AS TotalTime, c.credit AS Credit, CONCAT(i.first_name, ' ', i.last_name) AS Instructor \r\n" +
					"FROM dbo.students AS s\r\n" +
					"LEFT JOIN dbo.students_courses_xref AS scx ON s.pin = scx.student_pin\r\n" +
					"LEFT JOIN dbo.courses AS c ON scx.course_id = c.id\r\n" +
					"LEFT JOIN dbo.instructors AS i ON c.instructor_id = i.id\r\n" +
					"GROUP BY s.pin, s.first_name, s.last_name, s.time_created, c.name, c.total_time, c.credit, c.instructor_id, i.first_name, i.last_name\r\n", cn).ExecuteReader(), filelocation);
            }
        }

        private static string CreateCSV(IDataReader reader, string path)
        {
			string file = path + @"\report.csv";
			List<string> lines = new List<string>();

            string headerLine = "";

            string[] columns = new string[reader.FieldCount];

            for (int i = 0; i < reader.FieldCount; i++)
            {
                columns[i] = reader.GetName(i);
            }

            headerLine = string.Join(" | ", columns);
            lines.Add(headerLine);


            while (reader.Read())
            {
                object[] values = new object[reader.FieldCount];

                reader.GetValues(values);
                lines.Add(string.Join(" | ", values));
            }

            System.IO.File.WriteAllLines(file, lines);

            return file;
        }

        private static string GetConnectionString()
        {
            return @"Server=.;Database=coursera;Trusted_Connection=True;";
        }
    }

}
